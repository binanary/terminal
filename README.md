# Tiny Terminal

## About

Add useful terminal features to your project with this lightweight, drag-and-drop Go package.

Created by a Go beginner with a focus on simplicity to ensure everyone can understand and build on it.

This is part of a larger personal project, so all updates will be based on the roadmap for that project. You should make a fork if you intend to:
- alter existing functions beyond optimizations and improvements
- add extensive functionality that would warrant a standalone module
- make contributions that you do not want released into the public domain

For that reason, this is published as a package (rather than a module) by design. It is intended to provide other developers looking to implement similar features with a starting point rather than a fully-featured solution.

## Installation

You can use this package by copying the .go files to your project, either manually or using `git clone`. Cloning this repo directly is not recommended, as breaking changes may be introduced at any time and without warning.

Once the files are in your project, you should be able to locally import the package into any of your files.

## Usage

See comments above each exported object in the source code.

## Support

Feel free to submit an issue if you need help or find a bug.

## Contributing

Due to the nature of this project, merge requests will likely only be accepted in the following cases:
- The changes optimize or improve an existing function without changing overall functionality
- The changes implement a new feature that fits extremely well with existing features

## License

This project is intended to be public domain. Since the legal feasibility of that is unclear in some regions, the [Unlicense](./LICENSE) is used as a close approximation of that.
