package terminal

import (
	"fmt"
	"os"

	"golang.org/x/term"
)

var prefix = "\x1b["

func keypress(length int) []byte {
	oldState, err := term.MakeRaw(int(os.Stdin.Fd()))
	if err != nil {
		panic(err)
	}
	defer term.Restore(int(os.Stdin.Fd()), oldState)
	bs := make([]byte, length)
	os.Stdin.Read(bs)
	return bs
}

// Pause execution until the user hits any key
func AwaitAnyKey() {
	keypress(1)
}

// Intercepts the user's next keypress and returns the string representation of the key
func GetNextKey() (res string) {
	bs := keypress(3)

	switch bs[0] {
	case 3:
		fmt.Print("^C\n")
		os.Exit(130)
	}

	emptyByte := byte('\x00')
	res += string(bs[0])
	// Use if this func is ever changed to accept a high or arbitrary number of characters
	/* 	for _, b := range bs[1:] {
		if b == emptyByte {
			break
		}
		res += string(b)
	} */
	if bs[1] != emptyByte {
		res += string(bs[1])
		if bs[2] != emptyByte {
			res += string(bs[2])
		}
	}

	// Consider accepting a struct to do actions for certain keypresses
	return
}

// Removes all visible content from the terminal (equivalent to using the "clear" command)
func Clear() {
	fmt.Print(prefix + "2J")
}

// Clears the cursor's current line in the terminal and places the cursor at the leftmost position in that line
func ClearLine() {
	fmt.Print(prefix + "2K" + "\r")
}

// Uses the terminal's bell feature to get the user's attention
func Notify() {
	fmt.Print("\x07")
}

// Ends any currently-set text formatting, including styles and colors
func ResetFormatting() {
	fmt.Print(prefix + "0m")
}

// Moves the viewport (and cursor) up n lines.
// Useful for removing an unwanted new line added by the enter key.
func ScrollUp(n int) {
	fmt.Print(prefix, n, "T")
}

// Cursor

type cursor struct {
	MoveUp     func(n int) // Move the cursor up n lines
	MoveDown   func(n int) // Move the cursor down n lines
	MoveLeft   func(n int) // Move the cursor left n spaces
	MoveRight  func(n int) // Move the cursor right n spaces
	GoToLine   func(n int) // Counting from line 0 at the top of the visible terminal, move the cursor to the nth line
	visibility bool
}

// Functions that affect the cursor
var Cursor = cursor{
	MoveUp:     func(n int) { fmt.Print(prefix, n, "A") },
	MoveDown:   func(n int) { fmt.Print(prefix, n, "B") },
	MoveLeft:   func(n int) { fmt.Print(prefix, n, "D") },
	MoveRight:  func(n int) { fmt.Print(prefix, n, "C") },
	GoToLine:   func(n int) { fmt.Print(prefix, n, ";0H") },
	visibility: true,
}

func (c *cursor) Hide() {
	fmt.Print(prefix, "?25l")
	c.visibility = false
}

func (c *cursor) Show() {
	fmt.Print(prefix, "?25h")
	c.visibility = true
}

// Return the cursor to the beginning of the next line and make it visible
func (c cursor) Reset() {
	fmt.Print("\n\r")
	if !c.visibility {
		c.Show()
	}
}
